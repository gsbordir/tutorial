push local repository ke GIT
- buka gitbash
  - cd ke project yang mau di-push ke GIT
  - ketik "git init" -> untuk inisialiasi project sebagai project git supaya bisa dibaca oleh source tree juga
  - ketik "git add ."
  - ketik "git commit -m "first commit""
    kalau muncul error untuk setting email dan username, maka ikutin aja petunjuknya.
    email dan username sesuai bitbucket account
      git config --global user.email "you@example.com" -> misal git config --global user.email "joe@gmail.com"
      git config --global user.name "Your Name" -> misal git config --global user.name "joe"
    bisa juga menggunakan autentikasi ssh dan ssh-agent, bisa baca tutorialnya di -> setting ssh-agent for gitbash
- kalau gagal masalah permission, maka gitbash nya diclose aja
  - run gitbash sebagai administrator lewat windows search
  - cd ke project yang akan di-push
  - ulangi lagi langkah git config setting usernya
  - kalau da berhasil ulangi lagi langkah commitny
- ke akun bitbucket / git
  - buat repository baru
  - pilih NO untuk opsi include a README option
  - pilih NO untuk include .gitignore
  - buat repository-nya
- kembali ke gitbash terminal
  - ketik "git remote add origin url" -> urlnya bisa di copy dari bitbucket step 2 (bisa diklik repository yang sudah dibuat tadi, ada step 2 di-copy aja)
    contoh "git remote add origin git@bitbucket.org:gsbordir/cashier-view.git
  - ketik "git push -u origin master"
- repository lokal da berhasil ke push ke repository yang ada di bitbucket
- di source tree, klik tab +
- pilih add
- cari project yang ingin dibuka di source tree. misalkan project yang barusan di push
  hanya project yang sudah di init (git init) sebagai git project yang bisa dibuka source tree
- kalau ga mau pakai gitbash, bisa pakai gui source tree untuk bantu kamu push project ke remote/bitbucket
- tutorial penggunaan source tree bisa cari di inet