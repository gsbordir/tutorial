1. install JDK dan JRE
   - https://www.oracle.com/java/technologies/javase-jdk13-downloads.html
   - setting JAVA_HOME
     - ke windows search, cari advanced system settings
     - ke tab advanced
     - klik environment varibles
     - di kolom system variables, cek ada JAVA_HOME atau tidak
       kalau tidak ada maka klik new
     - variable name diisi dengan JAVA_HOME
     - variable value di-browse directory aja, arahkan ke folder JDK di install
       default folder jdk biasanya di C:\Program Files\Java\
     - habis itu di ok saja
     - tambahkan JAVA_HOME yang barusan dibuat ke dalam Path
       - masih di kolom system variables
       - cari Path
       - klik edit
       - klik new
       - ketik %HAVA_HOME%\bin
       - klik ok
       - klik ok lagi

2. install maven
   - bikin folder opt di drive C, install mavenny di folder opt ini
   - https://mkyong.com/maven/how-to-install-maven-in-windows/
   - setting MAVEN_HOME
   - ke windows search, cari advanced system settings
     - ke tab advanced
     - klik environment varibles
     - di kolom system variables, cek ada MAVEN_HOME atau tidak
       kalau tidak ada maka klik new
     - variable name diisi dengan MAVEN_HOME
     - variable value di-browse directory aja, arahkan ke folder MAVEN di install
       sesuai langkah install maven sebelumnya berarti di C:\opt\
     - habis itu di ok saja
     - tambahkan MAVEN_HOME yang barusan dibuat ke dalam Path
       - masih di kolom system variables
       - cari Path
       - klik edit
       - klik new
       - ketik %MAVEN_HOME%\bin
       - klik ok
       - klik ok lagi

3. install GIT -> buat project version controlling. ada git bash juga buat jalankan linux command
   - https://git-scm.com/book/en/v2
   
   buat alias untuk maven clean install menjadi mci
   1. ke folder git/etc
   2. edit profile (harus punya permission administrator)
   3. tambahan HOME=${GIT_BASH_HOME} di paling atas setelah comment "# To learn more about startup files, refer to your shell's man page."
   4. ke windows search "view addvance system settings"
   5. klik environment variables
   6. tambahan GIT_BASH_HOME
      - klik new
      - variable name isi GIT_BASH_HOME
      - klik browse directory. arahkan ke C:\Program Files\Git\etc
   7. ok
   8. windows search buka git bash (run as administrator)
   9. ketik echo $HOME
  10. harusny print C:\Program Files\Git\etc
  11. ketik pwd (harusny directoryny masih /c/Users/ASUS (biarkan aja ga apa2, .bash_history masih kebuat otomatis di folder sini)
  12. sekarang cd (change directory) ke C:\Program Files\Git\etc
  13. buat file .bash_profile (touch .bash_profile)
  14. buka bash_profile nya
  15. tambahkan (alias mci="mvn clean install") lalu simpan dan closed
  16. tutup git bash nya, lalu buka lagi
  17. ketik mci harusnya sudah kebaca sebagai maven clean install. selesai

  cara stash, commit, dan push lewat terminal git bash:
  1. ke tab project yang akan di push perubahannya
  2. klik open terminal
  3. ketik command "git add ." -> ini untuk add / stash seluruh file yang sudah diubah
  4. git commit -m "comment apa aja"
  5. kalau muncul error untuk setting email dan username, maka ikutin aja petunjuknya.
     email dan username sesuai bitbucket account
     git config --global user.email "you@example.com" -> misal git config --global user.email "joe@gmail.com"
     git config --global user.name "Your Name" -> misal git config --global user.name "joe"
  6. kalau gagal masalah permission, maka gitbash nya diclose aja
     - run gitbash sebagai administrator lewat windows search
     - cd ke project yang akan di-push
     - ulangi langkah no.5 dan 4
  7. kalau da berhasil commit, ketik command "git push" untuk push ke repo yang ada di bitbucket

  // kalau mau menggunakan autentikasi dengan ssh key
  setting ssh untuk git / bitbucket:
  - generate ssh key dulu:
    - open gitbash
    - type "ssh-keygen"
    - kalau ga mau ganti lokasi penyimpanannya maka lgsg dienter aja
    - input passphrase - kalau ga mau pakai password lgsg dienter aja
    - confirm passphrase - kalau ga ada input passphrase maka dienter aja
    - ssh key sudah terbuat di folder default tadi. misal: (C:/Users/Asus/.ssh)
    - di folder .ssh tersebut ada file:
      - id_rsa -> ini yang private
      - id_rsa.pub -> ini yang public
  - add public key id_rsa.pub ke bitbucket
    - ke bitbucket
    - ke icon profile, pilih bitbucket setting
    - di bawah tab security, klik ssh keys
    - add key
    - label diisi apa aja. cth: default public key
    - ke file id_rsa.pub tadi, copy isi yang ada di dalam file tersebut
    - balik ke bitbucket, paste isinya ke key
    - lalu klik ok / save / add key
  - supaya bisa push project menggunakan ssh
    - balik ke gitbash
    - ketik "eval $(ssh-agent)" -> ini untuk menjalankan ssh agentnya
    - ketik ssh-add ~/.ssh/<private_key_file>
      misal ssh-add "C:/Users/Asus/.ssh/id_rsa" -> ini supaya ssh agentny bisa melakukan autentikasi ke gitbucket
    - ketik "ssh -T git@bitbucket.org"
      ini untuk cek apakah kita sudah berhasil login atau blm, kalau sudah dia akan print username bitbucket kita
      di bawah username kita kalau melihat pesan ini "You can use git or hg to connect to Bitbucket. Shell access is disabled" diabaikan aja dah, yg penting sudah berhasil login
    - kalau da berhasil login, berarti kita sudah bisa push project yang kita update ke repo bitbucket
  - kalau mau setiap kali menjalankan gitbash langsung otomatis menjalankan ssh-agent dan login
    - edit file .bash_profile yang sudah dibuat untuk bikin command alias di tutorial sebelumnya
    - atau bisa edit file .bashrc
    - copy data dibawah ini:

       env=~/.ssh/agent.env

       agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }

       agent_start () {
         (umask 077; ssh-agent >| "$env")
         . "$env" >| /dev/null ; }

       agent_load_env

       #add private key for ssh-agent
       ssh-add "C:/Users/ASUS/.ssh/id_rsa"

       # agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
       agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

       if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
         agent_start
         ssh-add
       elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
         ssh-add
       fi

       unset env

    - ssh-add yang ada di bawah comment #add private key for ssh-agent, itu lokasi id_rsa diedit sesuai lokasi id_rsa yg di generate tadi
    - sekarang setiap kali menjalankan git bash, otomatis ssh-agentnya akan lgsg jalan
    - untuk memastikannya bisa menjalankan command di bawah ini untuk cek apakah sudah login ke bitbucket atau belum:
      ketik "ssh -T git@bitbucket.org"
      kalau berhasil login maka seperti biasa akan mengeluarkan output username bitbucket


4. intall git gui (source tree)
   - https://www.sourcetreeapp.com/

5. bikin repository melalui source tree
   - bikin folder workspace di drive D / lainnya sesuai keinginan
   - bikin folder kosong misalkan xmember, tunjuk folder xmember tersebut sebagai repository
   - kalau da selesai coba bikin file di folder xmember yang tadi tunjuk
   - apabila file tersebut tidak ke detect sama sourcetree, di tengah2 sourcetree ada text "show in explorer", ini diklik saja
   - apabila tidak ada show in explorer di tengah2 sourcetree, bisa klik action lalu pilih show in explorer
   - liat url folder yang dibuka ketika show in explorer
   - apabila bukan di folder yang tadi kamu buat, maka cut saja folder xmember
   - pergi folder workspace yang tadi dibuat, lalu delete saja folder xmember
   - habis itu paste xmember yang tadi di cut
   - kembali ke sourcetree akan muncul error repository tidak ditemukan
   - minimize source treenya, hbs itu buka lagi
   - klik saja tanda "?" / klik 2 kali di error tab, lalu pilih change folder
   - arahkan ke folder xmember yang tadi di cut
   - setelah itu coba buat file lagi di folder xmember tersebut
   - kembali ke source tree harusnya akan ke detect file yang tadi barusan dibuat

   set git bash sebagai default terminal di souce tree:
   - di source tree, pilih menu tools
   - klik option
   - ke tab git
   - centang "use git bash as default terminal"
   - ok
  
6. download STS untuk programmingnya
   download Spring Tools 4 for Eclipse untuk windows 64-bit (sesuaikan spesifikasi laptop)
   - https://spring.io/tools
   ini installnya dalam bentuk JAR. dijalankan aja. nanti akan ke extract folder sts nya, di dalamnya ada .exe
   - buat folder "workspace" taruh di drive D
   - pilih folder workspace tersebut untuk dijadikan sts workspace.
     ini supaya project2 yang ada di workspace ini ke baca sama sts

7. kalau menjalankan STS muncul Error could not find tools.jar, lakukan langkah berikut:
    - ke folder SpringToolSuite4 (STS)
    - ubah SpringToolSuite4.ini
    - tambahkan data ini di paling awal (untuk path bisa copy dari Window Explorer path, karena jdk sesuai yang diinstall):
      -vm
      C:\Program Files\Java\jdk-13.0.2\bin\javaw
      C:\Program Files\Java\jdk-13.0.2
    - bisa baca juga tutorialnya di sini:
      https://www.codetd.com/en/article/7021840

8. install mongodb
   - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
   - installnya custom aja
   - install mongodb compassnya juga aja (ini buat GUI)
   - kalau stuck (ga jalan-jalan install mongodb compassnya di cancel aja)
   - kalau dicancel masih ga ada response apa2, ke "ctrl+shift+esc", close saja installernya
   - run lagi saja mongodb installernya
     - kalau dapat error "another installation is in progress", maka ke "ctrl+shift+esc", di-end saja "windows installer" yang sedang berjalan
   - coba run lagi mongodb installernya
   - kalau da ok, pilih custom, mongodb compassnya  di uncheck saja, nanti diinstall setelah mongodb berhasil diinstall
   - keganjalan:
     - tadi begitu mongodb berhasil ke install, tiba2 saja mongodb compassnya juga keinstall dan lgsg nyala sendiri
     - ini kayaknya gara2 proses installernya yang sebelumnya sudah menginstall mongodb compassnya namun stuck setelah itu
     - jadi kalau da ke-install ya sudah ga usah install lagi mongodb compassny

   to start mongod.exe:
   - buat folder "data" di drive C
   - buat folder db di dalam folder data
   - mongod.exe sudah bisa distart (ini aplikasi kalau diclose maka koneksi ke database lgsg mati), bisa diakalin dengan menjalankan mongod-nya sebagai service
   atau bisa liat tutorialnya di sini kalau gagal start mongod.exe
   - https://stackoverflow.com/questions/20796714/how-do-i-start-mongo-db-from-windows

   menjalankan mongod sebagai service (biar mongod dijalankan di latar belakang)
   - buat folder "log" di C:\data
   - buat file mongod.log di folder log
   - copy file mongod.cfg dari "C:\Program Files\MongoDB\Server\4.2\bin\mongod.cfg" ke folder data
   - buat file "create mongod service.cmd" lalu edit dan isi file ini dengan (copy saja command di bawah ini bersama tanda "" juga):
     "C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --config "C:\data\mongod.cfg" --install
   - buat file untuk remove mongod service. kasih nama "remove mongod service.cmd", edit dan isi dengan (copy saja command di bawah ini bersama tanda "" juga):
     "C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --remove
   - buat file "start mongod.cmd" di desktop (biar mudah menjalankan mongod dari desktop), edit dan isi dengan:
     net start MongoDB
   - buat file "stop mongod.cmd" di desktop, edit dan isi dengan:
     net stop MongoDB
   
   cara menjalankan mongod sebagai service:
   - run as administrator "create mongod service.cmd" 1 kali saja untuk buat servicenya
   - run as administrator "start mongod.cmd", setelah itu mongod sudah nyala di latar belakang
     bisa diliat servicenya jalan atau ga lewat ctrl+shift+esc
     di bawah background and processes cek ada "MongoDB Database Server" ga, kalau ada berarti sudah jalan
   
   cara hentikan mongod service:
   - run as administrator "stop mongod.cmd"
     cek di ctrl+shift+esc, liat di bawah background and processes apakah "MongoDB Database Service" masih ada atau ga
     kalau ga ada berarti sudah berhenti

   cara menghapus mongod sevice:
   - run as administrator "remove mongod service.cmd"

   tips kalau service mongodnya mau dipakai terus:
   - run as administrator "create mongod service.cmd"
   - jangan jalankan "remove mongod service.cmd"
   - selanjutnya tinggal main run as administrator "start mongod.cmd" dan "stop mongod.cmd" saja

9. kalau STS muncul Error could not find tools.jar, lakukan langkah berikut:
    - ke folder SpringToolSuite4 (STS)
    - ubah SpringToolSuite4.ini
    - tambahkan data ini di paling awal (untuk path bisa copy dari Window Explorer path, karena jdk sesuai yang diinstall):
      -vm
      C:\Program Files\Java\jdk-13.0.2\bin\javaw
      C:\Program Files\Java\jdk-13.0.2
    - bisa baca juga tutorialnya di sini:
      https://www.codetd.com/en/article/7021840

10. create maven project
    - open STS
    - choose file
    - new project
    - choose maven, maven project
    - klik create a simple project (skip archetype selection)
    - next
    - fill project name, groupId, etc
    - finish

11. push local repository ke GIT
    - buka terminal di source tree
      - cd ke project yang mau di-push ke GIT
      - ketik "git init" -> untuk inisialiasi project sebagai project git supaya bisa dibaca oleh source tree juga
      - ketik "git add ."
      - ketik "git commit -m "first commit""
    - ke akun bitbucket / git
      - buat repository baru
      - pilih NO untuk opsi include a README option
      - buat repository-nya
    - kembali ke gitbash terminal
      - ketik "git remote add origin url" -> urlnya bisa di copy dari bitbucket step 2 (bisa diklik repository yang sudah dibuat tadi, ada step 2 di-copy aja)
        contoh "git remote add origin https://gsbordir@bitbucket.org/gsbordir/spring-mongo-example-with-maven.git"
      - ketik "git push -u origin master"
    - repository lokal da berhasil ke push ke repository yang ada di bitbucket
    - di source tree, klik tab +
    - pilih add
    - cari project yang ingin dibuka di source tree. misalkan project yang barusan di push
      hanya project yang sudah di init (git init) sebagai git project yang bisa dibuka source tree
    - kalau ga mau pakai gitbash, bisa pakai gui source tree untuk bantu kamu push project ke remote/bitbucket
    - tutorial penggunaan source tree bisa cari di inet